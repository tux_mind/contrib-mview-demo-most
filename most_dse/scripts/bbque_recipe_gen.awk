#!/usr/bin/awk -f
#
# Copyright (C) 2014 Politecnico di Milano
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

BEGIN {
	printf "<?xml version=\"1.0\"?>\n"
	printf "<BarbequeRTRM recipe_version=\"0.8\">\n"
	printf "\t<application priority=\"1\">\n"
	printf "\t\t<platform id=\"org.linux.cgroup\">\n"
	printf "\t\t\t<awms>\n"

	AWM_ID = 0
	FS = ","
}

{
	if (NR==1)
		next;

	AWM_VALUE = int($1)
	AWM_PE_QTY = int($2)

	printf "\t\t\t\t<awm id=\"%d\" name=\"cpuq%d\" value=\"%d\">\n", AWM_ID, AWM_PE_QTY, AWM_VALUE
	printf "\t\t\t\t\t<resources>\n"
	printf "\t\t\t\t\t\t<sys id=\"0\">\n"
	printf "\t\t\t\t\t\t\t<cpu id=\"0\">\n"
	printf "\t\t\t\t\t\t\t\t<pe qty=\"%d\"/>\n", AWM_PE_QTY
	printf "\t\t\t\t\t\t\t\t<mem units=\"Mb\" qty=\"200\"/>\n"
	printf "\t\t\t\t\t\t\t</cpu>\n"
	printf "\t\t\t\t\t\t</sys>\n"
	printf "\t\t\t\t\t</resources>\n"
	printf "\t\t\t\t</awm>\n"

	AWM_ID++
}

END {
	printf "\t\t\t</awms>\n"
	printf "\t\t</platform>\n"
	printf "\t</application>\n"
	printf "</BarbequeRTRM>\n"
	printf "\n"
	printf "<!-- vim: set tabstop=4 filetype=xml : -->\n"
}

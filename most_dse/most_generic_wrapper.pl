#!/usr/bin/perl

use XML::XPath;
use Getopt::Long; 

sub trim {$_[0] = (($_[0] =~ /^[ \t]*(.*?)[ \t]*$/s)[0]);}
sub print_xml_error { 
    ($output_xml_file, $reason, $fatal)=@_;
    open (METRICS_FILE, ">", $output_xml_file);
    print METRICS_FILE "<?xml version=\"1.0\" encoding =\"UTF-8\"?>\n";
    print METRICS_FILE "<simulator_output_interface xmlns=\"http://www.multicube.eu/\" version =\"1.3\">\n";
    print METRICS_FILE "\t<error reason=\"", $reason,"\" kind=\"$fatal\"/>\n";
    printf METRICS_FILE "</simulator_output_interface>\n";
    close (METRICS_FILE);
}

my $mgw_print_prefix=" --(MGW)--> ";
my $mgw_debug_prefix=" --(MGW-DEBUG)--> ";

my $err_getopt= GetOptions( "xml_system_configuration=s" => \$xml_sc, "xml_system_metrics=s" => \$xml_sm, "reference_xsd=s", \$ref_xsd , "debug", \$debug, "execution_config=s", \$execution_config, "timeout=s" => \$timeout);

if (!$execution_config){
    print $mgw_print_prefix, " UNDEFINED EXECUTION CONFIG\n";
    print_xml_error($xml_sm, "MGW:Undefined_execution_config", "fatal");
    exit 1;
}
if (!$xml_sc||!$xml_sm){
    print $mgw_print_prefix, " UNDEFINED XML INTERFACES\n";
    exit 1;
}

my $xp = XML::XPath->new(filename=>$xml_sc);
my $nodeset = $xp->find('*/parameter');

#CREATING SED FILE FOR TAG SUBSTITUTION
open (SED_FILE, '>_mgw_sedscript');
if ($nodeset->get_nodelist) {
    foreach my $node ($nodeset->get_nodelist) {
        if($debug){
            print  $mgw_debug_prefix , $node->find('@name'), " -- ", $node->find('@value') , "\n";
        }
        print SED_FILE "s?\@__MOST_GENERIC_WRAPPER__", $node->find('@name'), "__@?", $node->find('@value') , "?g\n";
#        if ($node->find('@name')->string_value() eq "ASIC1") {printf " ASIC found\n";}
#       here can be added metric transformations... eg resolution  in height and width
    }
}
else { 
    print $mgw_print_prefix, " WARNING UNMATCHED INPUT PARAMETERS\n";
    print_xml_error($xml_sm, "MGW:Unmatched_input_parameters", "fatal");
    close (SED_FILE);
    exit 1;
}
close (SED_FILE);

#TAG SUBSTITUTION IN ALL THE INPUT FILES
open (INPUT_TEMPLATE_FILE, $execution_config);
while (<INPUT_TEMPLATE_FILE>){
    chomp;
    @line_array_execution_config=split /\s+/,$_;
    if ($debug) {printf $mgw_debug_prefix, "@line_array_execution_config\n";}
    if ($line_array_execution_config[0] eq "#__MOST_GENERIC_WRAPPER__input_file__#"){
        if ($line_array_execution_config[1] eq $line_array_execution_config[2]){
            print ("ERROR: SAME INPUT and OUTPUT FILE!\n"); 
            exit 1;
        } 
        else{
            if ($debug) {print $mgw_debug_prefix, "sed -f _mgw_sedscript $line_array_execution_config[1] > $line_array_execution_config[2]\n";}
            system ("sed -f _mgw_sedscript $line_array_execution_config[1] > $line_array_execution_config[2]");
        }
    }
}

#RESOLVE EXECUTION CONFIG VARIABLES
system ("sed -f _mgw_sedscript $execution_config > mgw_execution_config.run");
system ("sed -i -e '/^#__MOST_GENERIC_WRAPPER__/d' mgw_execution_config.run");
system ("chmod a+x mgw_execution_config.run");
system ("rm -f _mgw_sedscript");


#EXECUTE with timeout management
if ($debug) {print $mgw_debug_prefix, " EXECUTE \n";}

#print "timeout= $timeout\n";

$execution_config_pid=fork();
if ($execution_config_pid==0) { 
    exec("./mgw_execution_config.run") or exit 1;
}
else{
    eval {
        local $SIG{ALRM} = sub { die "execution_timeout\n" }; # NB: \n required
        alarm $timeout;
#        print "BEFORE WAIT\n";
        waitpid($execution_config_pid,0);
        $ret_val=$? >> 8;
#        print "AFTER WAIT\n";
        alarm 0;
    };
    if ($@) {
        die unless $@ eq "execution_timeout\n";   # propagate unexpected errors
        # timed out
        kill 9, $execution_config_pid;
        if ($debug) {print $mgw_debug_prefix, " EXECUTION TIMEOUT $timeout ELAPSED\n";}
        print $mgw_print_prefix, " WARNING Execution Timeout\n";
        print_xml_error($xml_sm, "MGW:Execution_Timeout", "non-fatal");
        exit 1;
    }
    else {
        # didn't
        if ($debug) {print $mgw_debug_prefix, " EXECUTION TERMINATED BEFORE TIMEOUT\n";}
	}
}
if ($ret_val != 0) { 
    print $mgw_print_prefix, " WARNING Execution Failed\n";
    print_xml_error($xml_sm, "MGW:Execution_Failed", "non-fatal");
    exit 1;
}

#system("./mgw_execution_config.run"); #FARE CHECK RETURN VALUE
#if ($? != 0) { 
#    print $mgw_print_prefix, " WARNING Execution Failed\n";
#    print_xml_error($xml_sm, "MGW:Execution_Failed", "non-fatal");
#    exit 1;
#}

if ($debug) {print $mgw_debug_prefix, " PARSING OUTPUT FILES\n";}

#READ OUTPUT and create the XML_SYSTEM_METRICS file
open (METRICS_FILE, ">", $xml_sm);
print METRICS_FILE "<?xml version=\"1.0\" encoding =\"UTF-8\"?>\n";
print METRICS_FILE "<simulator_output_interface xmlns=\"http://www.multicube.eu/\" version =\"1.3\">\n";
open (INPUT_TEMPLATE_FILE, $execution_config);
while (<INPUT_TEMPLATE_FILE>){
    chomp;
    @line_array_execution_config=split /\s+/,$_;
    if ($line_array_execution_config[0] eq "#__MOST_GENERIC_WRAPPER__output_file__#"){
        if ($debug) {printf $mgw_debug_prefix, "$line_array_execution_config[1]\n";}
        $metric_name=$line_array_execution_config[1];
        my $metric_matched=0;
        if ($line_array_execution_config[3] eq "coordinate"){
            $coordinate_row=$line_array_execution_config[4];
            $coordinate_column=$line_array_execution_config[5];
            $delimiter=$line_array_execution_config[6];
            open (EXEC_OUTPUT_FILE, $line_array_execution_config[2]);
            while (<EXEC_OUTPUT_FILE>){
                chomp;
                $coordinate_row-=1;
                if ($coordinate_row==0){
                    if ($delimiter){
                        if (($delimiter eq ";") || ($delimiter eq ",")) {
                            @line_array_output_file=split($delimiter,$_);
                        }
                        else {
                            print $mgw_print_prefix, " WARNING UNSUPPORTED DELIMITER FOR $metric_name\n"; 
                            close (METRIC_FILE);
                            close (EXEC_OUTPUT_FILE);
                            print_xml_error($xml_sm, "MGW:Unsupported_delimiter", "fatal");
                            exit 1;
                        }
                    }
                    else{@line_array_output_file=split /\s+/,$_;}
                    print METRICS_FILE "\t<system_metric name=\"$metric_name\" value=\"", trim($line_array_output_file[($coordinate_column-1)]) , "\"/>\n";
                    $metric_matched=1;
                }
            }
            close (EXEC_OUTPUT_FILE);
        }
        elsif ($line_array_execution_config[3] eq "xpath"){
            my $XP_EXEC_OUTPUT_FILE = XML::XPath->new(filename=>$line_array_execution_config[2]);
            my $xpath_value= $XP_EXEC_OUTPUT_FILE->find($line_array_execution_config[4]);
            print METRICS_FILE "\t<system_metric name=\"$metric_name\" value=\"$xpath_value\"/>\n";
            $metric_matched=1;
        }
        elsif ($line_array_execution_config[3] eq "template"){
            my $string= trim($line_array_execution_config[4]);
            $string =~ s/(\W)/\\$1/g; #ADDS \ to special characters
            open (EXEC_OUTPUT_FILE, $line_array_execution_config[2]);
            while (<EXEC_OUTPUT_FILE>){
                chomp;
                if($_ =~ m/$string\s*(\S+)/) {
                    my $template_value=$1;
                    print METRICS_FILE "\t<system_metric name=\"$metric_name\" value=\"$template_value\"/>\n";
                    $metric_matched=1;
                }
            }
            close (EXEC_OUTPUT_FILE);
        }
        elsif ($line_array_execution_config[3] eq "regexp"){ #to be tested
            my $string= trim($line_array_execution_config[4]);
            open (EXEC_OUTPUT_FILE, $line_array_execution_config[2]);
            while (<EXEC_OUTPUT_FILE>){
                chomp;
                if($_ =~ m/$string/) {
                    my $regexp_value=$1;
                    print METRICS_FILE "\t<system_metric name=\"$metric_name\" value=\"$regexp_value\"/>\n";
                    $metric_matched=1;
                    #print "\t<system_metric name=\"$metric_name\" value=\"$regexp_value\"/>\n";
                }
            }
            close (EXEC_OUTPUT_FILE);
        }
        else {
            print $mgw_print_prefix, " WARNING UNDEFINED TYPE\n";
            close (METRICS_FILE);
            print_xml_error($xml_sm, "MGW:Unsupported_parsing_type", "fatal");
            exit 1;
        }
        if ($metric_matched==0) {
            print $mgw_print_prefix, " WARNING METRIC ", $metric_name, " UNMATCHED\n";
            close (METRICS_FILE);
            print_xml_error($xml_sm, "MGW:Unmatched_metrics", "non-fatal");
            exit 1;
        }
    }
}
printf METRICS_FILE "</simulator_output_interface>\n";
close (METRICS_FILE);

exit 0; 



/**
 *       @file  MviewDemoMost_exc.h
 *      @brief  The OpenMP-MView-MOST BarbequeRTRM application
 *
 * Description: OpenMP implementation of the Stereo-Matching algorithm described
 *              in the article: Ke Zhang, Jiangbo Lu and Gauthier Lafruit,
 *              "Cross-Based Local Stereo Matching Using Orthogonal Integral
 *              Images", IEEE Transactions on Circuits and Systems for Video
 *              Technology, Vol. 19, no. 7, July 2009.
 *
 *     @author  Edoardo Paone, edoardo.paone@polimi.it
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 2014, Politecnico di Milano
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#ifndef MVIEW_DEMO_MOST_EXC_H_
#define MVIEW_DEMO_MOST_EXC_H_

#include <mutex>

#include <opencv/cv.h>
#include <opencv/highgui.h>

#include <bbque/bbque_exc.h>

using bbque::rtlib::BbqueEXC;


/* Stereo-Match data types */
typedef struct {                                                                
    unsigned char R, G, B;                                                      
} rgb_t; 

typedef struct {
    int l, r, u, d;
} cross_t;

typedef struct {
    unsigned char disparity;
    int costo;
} vote_t;

typedef struct
{
    unsigned int bitnum[8];
    unsigned int npixels;
} dpr_t;


class MviewEXC : public BbqueEXC {

public:

	MviewEXC(
		std::string const & name,
		std::string const & recipe,
		RTLIB_Services_t *rtlib,
		std::string const & datapath,
		int grayscale);

	void SetOpParams(
		int max_hypo_value,
		int hypo_step,
		int max_arm_length,
		int color_threshold,
		int matchcost_limit,
		int num_threads);

	void ExitLoop() {
		std::lock_guard<std::mutex> lock(exit_mutex);
		exit_loop = true;
	}

private:
	/* BBQ EXC virtual methods */
	RTLIB_ExitCode_t onSetup();
	RTLIB_ExitCode_t onConfigure(uint8_t awm_id);
	RTLIB_ExitCode_t onRun();
	RTLIB_ExitCode_t onRelease();

	/* StereoMatch methods */
	void winBuild(cross_t * crosses, const rgb_t *bitmap);
	void raw_horizontal_integral(const rgb_t *bitmapLx, const rgb_t *bitmapRx, int disp_cur);
	void horizontal_integral(int disp_cur);
	void vertical_integral(int disp_cur);
	void crossregion_integral(int disp_cur);
	void refinement(unsigned char *disparity);

	/* Flag to exit for ExC loop */
	bool exit_loop;

	/* Mutex to protext exit_loop */
	std::mutex exit_mutex;

	/* Dataset parameters */
	const std::string datapath;
	const int grayscale;

	/* Frame size */
	int imgwidth;
	int imgheight;

	/* StereoMatch parameters */
	int max_hypo_value;
	int hypo_step;
	int max_arm_length;
	int color_threshold;
	int matchcost_limit;
	int num_threads;

	/* StereoMatch buffers */
	cross_t *crosses_lx;
	cross_t *crosses_rx;
	int *raw_matchcost;
	int *matchcost;
	int *anchorreg;
	int *fmatchcost;
	int *fanchorreg;
	vote_t *dpr_votes;
	dpr_t *dpr_block;

	/* Disparity map */
	cv::Mat outMap;
	cv::Mat refMap;
};

#endif // MVIEW_DEMO_MOST_EXC_H_


/**
 *       @file  MviewDemoMost_main.cc
 *      @brief  The OpenMP-MView-MOST BarbequeRTRM application
 *
 * Description: OpenMP implementation of the Stereo-Matching algorithm described
 *              in the article: Ke Zhang, Jiangbo Lu and Gauthier Lafruit,
 *              "Cross-Based Local Stereo Matching Using Orthogonal Integral
 *              Images", IEEE Transactions on Circuits and Systems for Video
 *              Technology, Vol. 19, no. 7, July 2009.
 *
 *     @author  Edoardo Paone, edoardo.paone@polimi.it
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 2014, Politecnico di Milano
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#include <iostream>
#include <signal.h>

#include <bbque/utils/logging/logger.h>
#include <bbque/utils/utility.h>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>

#include "MviewDemoMost_exc.h"
#include "version.h"

// Setup logging
#undef  BBQUE_LOG_MODULE
#define BBQUE_LOG_MODULE "OmpMview"

#define BORDER 18

namespace bu = bbque::utils;
namespace po = boost::program_options;

/**
 * @brief A pointer the logger provided by RTLib
 */
std::unique_ptr<bu::Logger> logger;

/**
 * @brief A pointer to an EXC
 */
typedef std::shared_ptr<MviewEXC> pBbqueEXC_t;

/**
 * The decription of each MviewDemoMost parameters
 */
po::options_description opts_desc("OpenMP-MView-MOST Configuration Options");

/**
 * The map of all MviewDemoMost parameters values
 */
po::variables_map opts_vm;

/**
 * The services exported by the RTLib
 */
RTLIB_Services_t *rtlib;

/**
 * @brief The recipe to use for all the EXCs
 */
std::string recipe;

/**
 * @brief The EXecution Context (EXC) registered
 */
pBbqueEXC_t pexc;

/**
 * @brief Application EXC name
 */
std::string exc_name("MviewEXC");

/**
 * @brief Application EXC id
 */
int exc_id;

/**
 * @brief The application configuration file
 */
std::string conf_file = BBQUE_PATH_PREFIX "/" BBQUE_PATH_CONF "/MviewDemoMost.conf" ;

/**
 * @brief The dataset path
 */
std::string dataset_path = BBQUE_PATH_PREFIX "/../contrib/user/mview-demo-most/dataset/Aloe" ;

/**
 * @brief Flag to profile configuration with command-line parameters
 */
bool use_cmd_options = false;


void my_handler(int s){
	std::cout << std::endl << std::flush;
	logger->Warn("Caught signal %d, terminating the ExC", s);
	pexc->ExitLoop();
}

void ParseCommandLine(int argc, char *argv[]) {
	// Parse command line params
	try {
	po::store(po::parse_command_line(argc, argv, opts_desc), opts_vm);
	} catch(...) {
		std::cout << "Usage: " << argv[0] << " [options]\n";
		std::cout << opts_desc << std::endl;
		::exit(EXIT_FAILURE);
	}
	po::notify(opts_vm);

	// Check for help request
	if (opts_vm.count("help")) {
		std::cout << "Usage: " << argv[0] << " [options]\n";
		std::cout << opts_desc << std::endl;
		::exit(EXIT_SUCCESS);
	}

	// Check for version request
	if (opts_vm.count("version")) {
		std::cout << "OpenMP-MView-MOST (ver. " << g_git_version << ")\n";
		std::cout << "Copyright (C) 2011 Politecnico di Milano\n";
		std::cout << "\n";
		std::cout << "Built on " <<
			__DATE__ << " " <<
			__TIME__ << "\n";
		std::cout << "\n";
		std::cout << "This is free software; see the source for "
			"copying conditions.  There is NO\n";
		std::cout << "warranty; not even for MERCHANTABILITY or "
			"FITNESS FOR A PARTICULAR PURPOSE.";
		std::cout << "\n" << std::endl;
		::exit(EXIT_SUCCESS);
	}

	// EXC name, using application instance ID
	if (opts_vm.count("instance")) {
		exc_name = "MviewEXC_I"+std::to_string(exc_id);
	}
	else {
		exc_name = "MviewEXC";
	}

	// Check for profiling request
	if (opts_vm.count("profile")) {
		use_cmd_options = true;
	}
	else {
		use_cmd_options = false;
	}
}

int main(int argc, char *argv[]) {

	/*
	 * Configuration Options
	 */
	int max_hypo_value;
	int hypo_step;
	int max_arm_length;
	int color_threshold;
	int matchcost_limit;
	int num_threads;
	int grayscale;

	opts_desc.add_options()
		("help,h", "print this help message")
		("version,v", "print program version")
		("profile,p", "use command-line parameters")

		("conf,c", po::value<std::string>(&conf_file)->
			default_value(conf_file),
			"application configuration file")
		("recipe,r", po::value<std::string>(&recipe)->
			default_value("MviewDemoMost"),
			"recipe name (for all EXCs)")
		("instance,i", po::value<int>(&exc_id),
			"instance id used for the ExC name")

		("datapath,d", po::value<std::string>(&dataset_path)->
			default_value(dataset_path),
			"the path of input dataset")
		("grayscale,g", po::value<int>(&grayscale)->
			default_value(3),
			"intensity scaling for disparity map")

		("max_hypo_value,m", po::value<int>(&max_hypo_value)->
			default_value(90),
			"max disparity hypothesis")
		("hypo_step,s", po::value<int>(&hypo_step)->
			default_value(1),
			"hypothesis loop step")
		("max_arm_length,a", po::value<int>(&max_arm_length)->
			default_value(16),
			"support region span")
		("threshold,t", po::value<int>(&color_threshold)->
			default_value(30),
			"pixel intensity threshold")
		("matchcost_limit,l", po::value<int>(&matchcost_limit)->
			default_value(60),
			"matching cost upper limit")
		("omp_num_threads,n", po::value<int>(&num_threads)->
			default_value(0),
			"number of OpenMP threads")
	;

	// Parse command line options
	ParseCommandLine(argc, argv);

	// Setup a logger
	bu::Logger::SetConfigurationFile(conf_file);
	logger = bu::Logger::GetLogger("MviewEXC");

	// Welcome screen
	logger->Info(".:: OpenMP-MView-MOST (ver. %s) ::.", g_git_version);
	logger->Info("Built: " __DATE__  " " __TIME__ "");


	// Initializing the RTLib library and setup the communication channel
	// with the Barbeque RTRM
	logger->Info("STEP 0. Initializing RTLib, application [%s]...",
			::basename(argv[0]));
	RTLIB_Init(::basename(argv[0]), &rtlib);
	assert(rtlib);


	logger->Info("STEP 1. Registering EXC [%s] using [%s] recipe...",
			exc_name.c_str(), recipe.c_str());
	pexc = pBbqueEXC_t(new MviewEXC(exc_name, recipe, rtlib,
			dataset_path, grayscale));
	if (!pexc->isRegistered())
		return RTLIB_ERROR;

	if (use_cmd_options) {
		logger->Info("STEP 1.1. Configuring EXC using command-line parameters...");
		pexc->SetOpParams(
			max_hypo_value,
			hypo_step,
			max_arm_length,
			color_threshold,
			matchcost_limit,
			num_threads);
	}

	// Exit the EXC when CTRL+C is pressed
	signal (SIGINT, my_handler);

	logger->Info("STEP 2. Starting EXC control thread...");
	pexc->Start();


	logger->Info("STEP 3. Waiting for EXC completion...");
	pexc->WaitCompletion();


	logger->Info("STEP 4. Disabling EXC...");
	pexc = NULL;

	if (use_cmd_options) {
		unsigned int PixErrTotal = 0;
		unsigned int PixNumTotal = 0;

		cv::Mat outMap = cv::imread(dataset_path+"/i-out.bmp", CV_LOAD_IMAGE_GRAYSCALE);
		if (!outMap.data) {
			logger->Error("Failed to open output disparity map!");
			return RTLIB_ERROR;
		}

		cv::Mat refMap = cv::imread(dataset_path+"/ref_rx.bmp", CV_LOAD_IMAGE_GRAYSCALE);
		if (!refMap.data) {
			logger->Error("Failed to open reference disparity map!");
			return RTLIB_ERROR;
		}

		if ( (outMap.size().height != refMap.size().height) ||
				(outMap.size().width != refMap.size().width) ) {
			logger->Error("Wrong size of reference and output disparity map!");
			return RTLIB_ERROR;
		}

		for (int y=(0+BORDER); y<(outMap.size().height-BORDER); y++)
		{
			for (int x=(0+BORDER); x<(outMap.size().width-BORDER); x++)
			{
				size_t PixIdx = y*outMap.size().width+x;

				PixErrTotal += abs((int)outMap.data[PixIdx] - (int)refMap.data[PixIdx]);
				PixNumTotal++;
			}
		}

		fprintf(stderr, "@avg_disparity_error=%lf@\n",
				PixErrTotal / (double)(PixNumTotal * grayscale));
	}

	logger->Info("===== OpenMP-MView-MOST DONE! =====\n");
	return EXIT_SUCCESS;
}

